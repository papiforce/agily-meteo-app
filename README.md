# Agily Météo App

## Quels sont les principes que tu as appliqué ?

J'ai débuté le développement de l'app en découpant les différents éléments de la page afin de savoir quels composants pourrait être réutilisé ou non.
Ensuite j'ai tenu à ce que le code soit bien structuré afin d'être plus facilement lisible.

## Peux-tu expliquer les décisions que tu as prise et pourquoi c'est la meilleure approche ?

J'ai décidé de prioriser le fonctionnement de l'app sans pour autant totalement abandonner le design/visuel. J'ai tout d'abord jeté un oeil sur l'API à utiliser ainsi que sa doc. J'ai commencé par éxécuter les requêtes afin d'obtenir les informations dont j'avais besoin pour le fonctionnement total de l'app. Et enfin j'ai fini par mettre en place le front. De plus j'ai choisi cette approche car le design se faisant à "l'oeil", n'ayant pas de maquettes avec des valeurs précises, je ne pouvais me permettre de m'attarder dessus.

## Quelles sont tes recommandations pour un travail futur ?

A vrai dire je n'ai pas bien compris la question.
