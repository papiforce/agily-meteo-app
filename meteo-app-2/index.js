const express = require("express");
const app = express();
const port = 5000;
const axios = require("axios");

app.get("/:position", async (req, res) => {
  const { position } = req.params;

  const cleanedData = [];

  await axios
    .get(
      `http://api.openweathermap.org/geo/1.0/direct?q=${position}&limit=1&appid=db988691faf182dfc3750cd1e57f3718`
    )
    .then(async (response) => {
      const data = response.data[0];

      await axios
        .get(
          `https://api.openweathermap.org/data/2.5/onecall?lat=${data.lat}&lon=${data.lon}&lang=${data.country}&exclude=current,hourly,minutely,alerts&units=metric&appid=db988691faf182dfc3750cd1e57f3718`
        )
        .then((subResponse) => {
          subResponse.data.daily.map((day, index) => {
            let {
              sunrise,
              sunset,
              moonrise,
              moonset,
              moon_phase,
              feels_like,
              dew_point,
              wind_deg,
              wind_gust,
              clouds,
              pop,
              rain,
              uvi,
              ...cleaned
            } = day;

            cleanedData.push(cleaned);
          });
        })
        .catch((err) => console.log(err));
    })
    .catch((err) => console.log(err));

  console.log(cleanedData);

  return res.status(200).send(cleanedData);
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
