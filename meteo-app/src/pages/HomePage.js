import { useState } from "react";
import styled from "styled-components";
import { useNavigate } from "react-router-dom";

import { theme } from "../core/Theme";

const Container = styled.div`
  background: ${theme.colors.greenPrimary};
  width: 100vw;
  height: 100vh;
`;

const Wrapper = styled.form`
  padding: 0 ${theme.spacing.two};
  width: 100%;
  max-width: 600px;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`;

const Title = styled.h1`
  margin: 0 auto ${theme.spacing.four};
  font-size: ${theme.fontSize.title0};
  line-height: 40px;
  max-width: 300px;
  background: linear-gradient(
    0deg,
    rgba(212, 232, 160, 1) 0%,
    rgba(43, 190, 92, 1) 100%
  );
  text-align: center;
  -webkit-background-clip: text;
  -webkit-text-fill-color: transparent;
`;

const Input = styled.input`
  padding: ${theme.spacing.two};
  font-size: ${theme.fontSize.large};
  border-radius: ${theme.borderRadius.big};
  color: ${theme.colors.greenPrimary};
  font-weight: ${theme.fontWeight.extraBold};

  box-shadow: none;
  outline: none;
  border: none;
  width: 100%;
`;

function HomePage() {
  const history = useNavigate();
  const [position, setPosition] = useState("");

  const getValueInfos = (e) => {
    e.preventDefault();
    history(`/weather/${position.toLowerCase()}`);
  };

  return (
    <Container>
      <Wrapper onSubmit={getValueInfos}>
        <Title>The Forecast Weather App</Title>
        <Input
          type="text"
          value={position}
          placeholder="Search"
          onChange={(e) => setPosition(e.target.value)}
        />
      </Wrapper>
    </Container>
  );
}

export default HomePage;
