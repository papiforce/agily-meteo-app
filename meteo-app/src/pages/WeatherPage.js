import { useState, useEffect } from "react";
import styled from "styled-components";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import moment from "moment";
import "moment/locale/fr";

import { theme } from "../core/Theme";
import { lessThan } from "../core/mediaQueries";

const Container = styled.div`
  padding: ${theme.spacing.four};
  background: grey;
  width: 100vw;
  height: 100vh;
  display: flex;

  ${lessThan("tablet")(`
    display: block;
    overflow-y: scroll;
  `)}
`;

const LeftWrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

const ArrowWrapper = styled.div`
  background: ${theme.colors.greenPrimary};
  font-size: ${theme.fontSize.display0};
  color: ${theme.colors.white};
  border-radius: ${theme.borderRadius.large};
  cursor: pointer;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 50px;
  height: 50px;
`;

const SelectedOneWrapper = styled.div`
  background: ${theme.colors.greenPrimary};
  border-radius: ${theme.borderRadius.big};
  padding: ${theme.spacing.one} ${theme.spacing.two};
  color: ${theme.colors.white};
  margin-top: auto;

  ${lessThan("tablet")(`
    margin: ${theme.spacing.four} 0; 
  `)}
`;

const WrapperHead = styled.div`
  gap: ${theme.spacing.three};
  display: flex;
  align-items: center;
`;

const WrapperBody = styled.div`
  gap: ${theme.spacing.three};
  display: flex;
  justify-content: space-around;
`;

const RightWrapper = styled.div`
  margin-left: auto;

  ${lessThan("tablet")(`
    overflow-y: scroll;
  `)}
`;

const WeatherCard = styled.div`
  background: ${theme.colors.greenPrimary};
  border-radius: ${theme.borderRadius.big};
  color: ${theme.colors.white};
  padding: ${theme.spacing.one} ${theme.spacing.two};
  gap: ${theme.spacing.three};
  display: flex;
  align-items: center;
  justify-content: space-between;
  cursor: pointer;
`;

const WeatherIcon = styled.img`
  height: 60px;
`;

const TempText = styled.p`
  color: ${theme.colors.white};
  font-size: ${theme.fontSize.display0};
  font-weight: ${theme.fontWeight.semiBold};
`;

const WeatherPage = () => {
  moment.locale("fr");
  const history = useNavigate();
  const [weatherData, setWeatherData] = useState();
  const [selectOne, setSelectOne] = useState();

  const goBackHome = () => {
    history("/");
  };

  const formatDayCapitalize = (day) => {
    return day[0].toUpperCase() + day.substring(1);
  };

  useEffect(() => {
    const path = window.location.pathname.split("/");
    const position = path[path.length - 1];

    axios
      .get(
        `http://api.openweathermap.org/geo/1.0/direct?q=${position}&limit=1&appid=db988691faf182dfc3750cd1e57f3718`
      )
      .then((res) => {
        const data = res.data[0];
        axios
          .get(
            `https://api.openweathermap.org/data/2.5/onecall?lat=${data.lat}&lon=${data.lon}&lang=${data.country}&exclude=current,hourly,minutely,alerts&units=metric&appid=db988691faf182dfc3750cd1e57f3718`
          )
          .then((response) => {
            setWeatherData(response.data.daily);
            setSelectOne(response.data.daily[0]);
          })
          .catch((subError) => console.log(subError));
      })
      .catch((err) => console.log(err));
  }, []);

  if (!weatherData || !selectOne) return <></>;

  console.log(weatherData);

  return (
    <Container>
      <LeftWrapper>
        <ArrowWrapper onClick={goBackHome}>{"<"}</ArrowWrapper>
        <SelectedOneWrapper>
          <WrapperHead>
            <WeatherIcon
              src={`https://openweathermap.org/img/wn/${selectOne.weather[0].icon}@4x.png`}
              alt="weather_icon"
            />
            <p
              style={{
                fontWeight: theme.fontWeight.bold,
                fontSize: theme.fontSize.display0,
              }}
            >
              {formatDayCapitalize(
                moment(selectOne.dt * 1000).format("dddd DD")
              )}{" "}
              {formatDayCapitalize(moment(selectOne.dt * 1000).format("MMM"))}
            </p>
          </WrapperHead>
          <WrapperBody>
            <div>
              <p
                style={{
                  fontWeight: theme.fontWeight.bold,
                }}
              >
                Jour - {selectOne.temp.day}°C
              </p>
              <p
                style={{
                  fontWeight: theme.fontWeight.bold,
                }}
              >
                Nuit - {selectOne.temp.night}°C
              </p>
              <p
                style={{
                  fontWeight: theme.fontWeight.bold,
                }}
              >
                Humidité - {selectOne.humidity}%
              </p>
            </div>
            <div>
              <p
                style={{
                  fontWeight: theme.fontWeight.bold,
                }}
              >
                Pression - {selectOne.pressure}hPa
              </p>
              <p
                style={{
                  fontWeight: theme.fontWeight.bold,
                }}
              >
                Vent - {selectOne.wind_speed}km/h
              </p>
            </div>
          </WrapperBody>
        </SelectedOneWrapper>
      </LeftWrapper>
      <RightWrapper>
        {weatherData.map((w, index) => {
          return (
            <WeatherCard
              key={`card_${index}`}
              onClick={() => setSelectOne(w)}
              style={{
                marginBottom:
                  index < weatherData.length ? theme.spacing.three : 0,
              }}
            >
              <WeatherIcon
                src={`https://openweathermap.org/img/wn/${w.weather[0].icon}@4x.png`}
                alt="weather_icon"
              />
              <div>
                <p
                  style={{
                    textAlign: "center",
                    fontWeight: theme.fontWeight.bold,
                    fontSize: theme.fontSize.display5,
                  }}
                >
                  {formatDayCapitalize(moment(w.dt * 1000).format("dddd"))}
                </p>
                <p style={{ textAlign: "center" }}>
                  {moment(w.dt * 1000).format("DD")}{" "}
                  {formatDayCapitalize(moment(w.dt * 1000).format("MMM"))}
                </p>
              </div>
              <TempText>{w.temp.day}°C</TempText>
            </WeatherCard>
          );
        })}
      </RightWrapper>
    </Container>
  );
};

export default WeatherPage;
