import React, { Fragment } from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import { ThemeProvider } from "styled-components";
import "./App.css";

import { theme } from "./core/Theme";

import HomePage from "./pages/HomePage";
import WeatherPage from "./pages/WeatherPage";

const App = () => {
  return (
    <ThemeProvider theme={theme}>
      <Router>
        <Fragment>
          <Routes>
            <Route exact path="/" element={<HomePage />} />
            <Route exact path="/weather/:city" element={<WeatherPage />} />
          </Routes>
        </Fragment>
      </Router>
    </ThemeProvider>
  );
};

export default App;
