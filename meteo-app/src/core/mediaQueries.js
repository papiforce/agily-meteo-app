import { theme } from "./Theme";

export const screens = {
  smallMobile: theme.screens.smallMobile,
  mobile: theme.screens.mobile,
  tablet: theme.screens.tablet,
  smallDesktop: theme.screens.smallDesktop,
  specialScreen: theme.screens.specialScreen,
  desktop: theme.screens.desktop,
  maxDesktop: theme.screens.maxDesktop,
};

export const lessThan = (key) => {
  return (style) => `@media (max-width: ${screens[key]}px) { ${style} }`;
};
